elements_amount = int(input("Please enter the maximum number of elements "
                            "to be sent: "))

max_package_weight = 20
min_elem_weight = 1
max_elem_weight = 10
element_number = 0
kilos_sent = 0
package_weight = 0
package_number = 0
lightest_package_weight = 20
lightest_package_number = 0

for element_number in range(elements_amount):
    element_weight = int(input("Please enter the element weight [kg]: "))
    if element_weight == 0:
        break
    elif element_weight < min_elem_weight or element_weight > max_elem_weight:
        print("WRONG WEIGHT OF THE ELEMENT!")
        break
    else:
        kilos_sent += element_weight
        if package_weight + element_weight <= max_package_weight:
            package_weight += element_weight
        else:
            package_number += 1
            if package_weight < lightest_package_weight:
                lightest_package_weight = package_weight
                lightest_package_number = package_number
            package_weight = element_weight
package_number += 1
if package_weight < lightest_package_weight:
    lightest_package_weight = package_weight
    lightest_package_number = package_number
sum_empty_kilograms = package_number * max_package_weight - kilos_sent
lightest_package_empty_kilo = max_package_weight - lightest_package_weight

print("\n")
print(f'Packages shipped: {package_number}')
print(f'Kilos shipped: {kilos_sent}')
print(f'The sum of empty kilos: {sum_empty_kilograms}')
print(f'Lightest package number: {lightest_package_number}')
print(f'Empty kilos in the lightest package: {lightest_package_empty_kilo}')
